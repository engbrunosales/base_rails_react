Rails.application.routes.draw do
  devise_for :users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  get '/rails/info/routes' => 'routes'
  get 'home/index'
  root 'home#index'
end
